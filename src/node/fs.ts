/*
 *  Utility functions related to filesystem.
 *  Created On 08 January 2021
 */

import fs from 'fs/promises'
import { handle } from '../generic/promise'

export const exists = async (path): Promise<boolean> => {
    const exists = await handle(fs.stat(path))

    if (exists.error) {
        return false
    } else {
        return true
    }
}
